package bt;

import java.util.ArrayList;
import java.util.List;

/**
 * The IndependenceTiplet object represents a data structure that contains the
 * variables involved in an independence query on data
 * 
 * @author fschluter
 */
public class IndependenceTriplet {

	/**
	 * Variable x of an independence triplet
	 */
	private List<Integer> x;

	/**
	 * Variable y of an independence triplet
	 */
	private List<Integer> y;

	/**
	 * Conditioning set of an independence triplet
	 */
	private List<Integer> z;

	/**
	 * Creates an IndependenceTriplet object
	 * 
	 * @param x
	 *            Variable x of an independence triplet
	 * @param y
	 *            Variable y of an independence triplet
	 * @param z
	 *            Conditioning set of an independence triplet
	 */
	public IndependenceTriplet(List<Integer> x, List<Integer> y, List<Integer> z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * Creates an IndependenceTriplet object
	 * 
	 * @param x
	 *            Variable x of an independence triplet
	 * @param y
	 *            Variable y of an independence triplet
	 * @param z
	 *            Conditioning set of an independence triplet
	 */
	public IndependenceTriplet(int x, int y, List<Integer> z) {
		this.x = new ArrayList<Integer>();
		this.x.add(x);
		this.y = new ArrayList<Integer>();
		this.y.add(y);
		this.z = z;
	}

	/**
	 * @return Variable x of an independence triplet
	 */
	public List<Integer> getX() {
		return x;
	}

	/**
	 * @param x
	 *            Variable x of an independence triplet
	 */
	public void setX(List<Integer> x) {
		this.x = x;
	}

	/**
	 * @return Variable Y of an independence triplet
	 */
	public List<Integer> getY() {
		return y;
	}

	/**
	 * @param y
	 *            Variable Y of an independence triplet
	 */
	public void setY(List<Integer> y) {
		this.y = y;
	}

	/**
	 * @return Conditioning set of an independence triplet
	 */
	public List<Integer> getZ() {
		return z;
	}

	/**
	 * @param z
	 *            Conditioning set of an independence triplet
	 */
	public void setZ(List<Integer> z) {
		this.z = z;
	}

	@Override
	public String toString() {
		return "<" + x + "," + y + "|" + z + ">";
	}

	public int size() {
		return x.size() + y.size() + z.size();
	}

}
