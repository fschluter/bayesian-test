package bt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple class representing a tabular dataset (train set)
 * 
 * @author fschluter
 */
public class Dataset {

	/**
	 * List of examples in the dataset. Only discrete datapoints.
	 */
	private List<List<Integer>> _examples;

	/**
	 * Number of variables. The size of the domain.
	 */
	private int _numberOfVariables;

	/**
	 * Array specifying the cardinalities of the variables of the domain.
	 */
	private int[] _cardinalities;

	/**
	 * Number specifying how many rows must be read from the file that contains
	 * the training examples.
	 */
	private int _maxNumberOfExamples;

	/**
	 * @param maxNumberOfExamples
	 *            Number specifying how many rows must be read from the file
	 *            that contains the training examples.
	 * @param cardinalities
	 *            Array specifying the cardinalities of the variables of the
	 *            domain.
	 */
	public Dataset(int maxNumberOfExamples, int[] cardinalities) {
		this._examples = new ArrayList<List<Integer>>(maxNumberOfExamples);
		this._cardinalities = cardinalities;
		this._numberOfVariables = 0;
		this._maxNumberOfExamples = maxNumberOfExamples;
	}

	/**
	 * Reads a tabular dataset (train set) from disk.
	 * 
	 * @param inputFile
	 *            Complete path of the file containing the training examples.
	 * @param numberOfExamples
	 *            Number specifying how many rows must be read from the file
	 *            that contains the training examples.
	 * @param skipFirstExamples
	 *            Number of rows to skip in the top of the file, before reading
	 *            the datapoints.
	 * @return An instance of the Dataset class.
	 * @throws IOException
	 */
	public Dataset readDataset(File inputFile, int numberOfExamples, int skipFirstExamples) throws IOException {
		Dataset dataset = new Dataset(numberOfExamples, _cardinalities);
		FileReader fileReader = new FileReader(inputFile);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		int numberOfVariables = 0;
		String line = null;
		for (int i = 0; i < skipFirstExamples; ++i)
			bufferedReader.readLine();

		while ((line = bufferedReader.readLine()) != null && dataset.getExamples().size() < _maxNumberOfExamples) {
			String[] items = line.trim().split(",");
			numberOfVariables = items.length;
			List<Integer> example = new ArrayList<Integer>(numberOfVariables);
			for (int i = 0; i < numberOfVariables; i++)
				example.add(i, Integer.parseInt(items[i]));
			dataset.addExample(example);
		}
		bufferedReader.close();

		dataset.setNumberOfVariables(numberOfVariables);
		return dataset;
	}

	/**
	 * Gets a portion of the current dataset, corresponding to the @param slice.
	 * 
	 * @param z
	 *            List of variables of the slice.
	 * @param slice
	 *            List of the values of the variables in the slice.
	 * @return An instance of the dataset with the datapoints that match whit
	 *         the input slice.
	 */
	public Dataset getSubdataset(List<Integer> z, int[] slice) {
		List<List<Integer>> matchingExamples = new ArrayList<List<Integer>>();
		for (List<Integer> example : _examples) {
			boolean match = true;
			for (int i = 0; i < z.size(); ++i) {
				int variable = z.get(i);
				int value = slice[i];
				if (example.get(variable) != value) {
					match = false;
				}
			}
			if (match) {
				matchingExamples.add(example);
			}
		}
		Dataset datasetOfSlice = new Dataset(matchingExamples.size(), _cardinalities);
		datasetOfSlice._examples = matchingExamples;
		return datasetOfSlice;
	}

	/**
	 * Returns the number of datapoints that match with the values of the
	 * variables given as input.
	 * 
	 * @param variables
	 *            List of variables to count.
	 * @param values
	 *            The values of the variables to count.
	 * @return
	 */
	public int getCounts(List<Integer> variables, int[] values) {
		int counts = 0;
		for (List<Integer> example : _examples) {
			boolean match = true;
			for (int i = 0; i < variables.size(); ++i) {
				int variable = variables.get(i);
				int value = values[i];
				if (example.get(variable) != value) {
					match = false;
				}
			}
			if (match)
				++counts;
		}
		return counts;
	}

	/**
	 * Adds an example to the dataset.
	 * 
	 * @param example
	 *            A list of integer in the form of a complete datapoint.
	 */
	public void addExample(List<Integer> example) {
		_examples.add(example);
	}

	/**
	 * Gets the ith example of the dataset.
	 * 
	 * @param index
	 *            Index of the example to get from the dataset.
	 * @return List of examples in the dataset. Only discrete datapoints.
	 */
	public List<Integer> getExample(int index) {
		return new ArrayList<Integer>(_examples.get(index));
	}

	/**
	 * @return A list of examples in the dataset. Only discrete datapoints.
	 */
	public List<List<Integer>> getExamples() {
		return _examples;
	}

	/**
	 * @return
	 */
	public int getNumberOfVariables() {
		return this._numberOfVariables;
	}

	/**
	 * @param numberOfVariables
	 */
	public void setNumberOfVariables(int numberOfVariables) {
		this._numberOfVariables = numberOfVariables;
	}

	/**
	 * @param card
	 */
	public void setCardinalities(int[] card) {
		this._cardinalities = card;
	}

	/**
	 * @return
	 */
	public int[] getCardinalities() {
		return this._cardinalities;
	}

	/**
	 * @param n
	 * @return
	 */
	public static int[] getBinaryDomain(int n) {
		int[] domain = new int[n];
		for (int i = 0; i < n; ++i) {
			domain[i] = 2;
		}
		return domain;
	}

	/**
	 * Test method for loading and showing by console a tabular dataset from
	 * disk.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String fullPathToDataset = "data/example4BinaryVariables.csv";
		int maxNumberOfExamples = 10; // read only first 10 examples
		Dataset dataset = new Dataset(maxNumberOfExamples, getBinaryDomain(4));
		try {
			dataset = dataset.readDataset(new java.io.File(fullPathToDataset), maxNumberOfExamples, 0);
			for (List<Integer> f : dataset.getExamples()) {
				System.out.println(f);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}