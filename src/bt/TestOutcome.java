package bt;

/**
 * Simple data structure to record the outcome of an independence test. This is
 * comparable by its log-probability.
 * 
 * @author fschluter
 * 
 */
public class TestOutcome implements Comparable<TestOutcome> {

	/**
	 * 
	 */
	protected IndependenceTriplet triplet;

	/**
	 * 
	 */
	public boolean truthValue;

	/**
	 * 
	 */
	private double loglikelihood;

	/**
	 * 
	 */
	private double logProbability;

	/**
	 * @param triplet
	 * @param truthValue
	 * @param loglikelihood
	 * @param logProbability
	 */
	public TestOutcome(IndependenceTriplet triplet, boolean truthValue, double loglikelihood, double logProbability) {
		super();
		this.triplet = triplet;
		this.truthValue = truthValue;
		this.loglikelihood = loglikelihood;
		this.logProbability = logProbability;
	}

	/**
	 * @return the triplet
	 */
	public IndependenceTriplet getTriplet() {
		return triplet;
	}

	/**
	 * @param triplet
	 *            the triplet to set
	 */
	public void setTriplet(IndependenceTriplet triplet) {
		this.triplet = triplet;
	}

	/**
	 * @return the truth_value
	 */
	public boolean isTruthValue() {
		return truthValue;
	}

	/**
	 * @param truth_value
	 *            the truth_value to set
	 */
	public void setTruthValue(boolean truth_value) {
		this.truthValue = truth_value;
	}

	/**
	 * @return the loglikelihood
	 */
	public double getLoglikelihood() {
		return loglikelihood;
	}

	/**
	 * @param loglikelihood
	 *            the loglikelihood to set
	 */
	public void setLoglikelihood(double loglikelihood) {
		this.loglikelihood = loglikelihood;
	}

	/**
	 * @return the logProbability
	 */
	public double getLogProbability() {
		return logProbability;
	}

	/**
	 * @param logProbability
	 *            the logProbability to set
	 */
	public void setLogProbability(double logProbability) {
		this.logProbability = logProbability;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(TestOutcome c) {
		final int BEFORE = -1;
		final int EQUAL = 0;
		final int AFTER = 1;
		// para ordenar según probabilidad de DEPENDENCIA (los más dependientes
		// van BEFORE)
		if (Math.exp(logProbability) > Math.exp(c.logProbability)) {
			return BEFORE;
		} else if (Math.exp(logProbability) < Math.exp(c.logProbability)) {
			return AFTER;
		} else {
			return EQUAL;
		}
	}
}
