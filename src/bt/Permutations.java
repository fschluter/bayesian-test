package bt;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple class for computing the permutations of a set of variables with
 * discrete cardinalities
 * 
 * @author fschluter
 */
public class Permutations {
	private static List<int[]> permutations;

	/**
	 * @param x
	 * @param cardinalities
	 * @return
	 */
	public static List<int[]> getPermutations(List<Integer> x, int[] cardinalities) {
		permutations = new ArrayList<int[]>();
		int[] n = new int[x.size()];
		int[] cards = new int[x.size()];

		for (int i = 0; i < x.size(); ++i) {
			cards[i] = cardinalities[x.get(i)];
		}

		getPermutationsRecursively(n, cardinalities, 0);
		return permutations;
	}

	/**
	 * @param vars
	 * @param cardinalities
	 * @param idx
	 */
	private static void getPermutationsRecursively(int[] vars, int[] cardinalities, int idx) {
		int[] variables = new int[vars.length];
		System.arraycopy(vars, 0, variables, 0, vars.length);

		/* stop condition for the recursion [base clause] */
		if (idx == variables.length) {
			permutations.add(variables);
			return;
		}
		for (int i = 0; i < cardinalities[idx]; i++) {
			variables[idx] = i;
			/* recursive invocation, for next elements */
			getPermutationsRecursively(variables, cardinalities, idx + 1);

		}
	}

}
