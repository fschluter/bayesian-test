package bt.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import bt.BayesianTest;
import bt.Dataset;

public class TestOfBayesianTest {
	public static void main(String[] args) {
		String fullPathToDataset = "data/example4BinaryVariables.csv";
		int maxNumberOfExamples = 10; // read only first 10 examples
		Dataset dataset = new Dataset(maxNumberOfExamples, Dataset.getBinaryDomain(4));
		try {
			dataset = dataset.readDataset(new java.io.File(fullPathToDataset), maxNumberOfExamples, 1);
			List<Integer> x = new ArrayList<Integer>();
			x.add(0);
			List<Integer> y = new ArrayList<Integer>();
			y.add(1);
			List<Integer> z = new ArrayList<Integer>();
			z.add(2);
			z.add(3);
			BayesianTest bt = new BayesianTest();
			bt.independent(dataset, x, y, z);
			System.out.println("loglikelihoods: " + bt.getLoglikelihoods()[0] + " , " + bt.getLoglikelihoods()[1]);
			System.out.println("logprobs  : " + bt.getLogProbs()[0] + " , " + bt.getLogProbs()[1]);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
