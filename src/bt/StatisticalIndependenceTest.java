package bt;

import java.util.List;

/**
 * This interface enables to implement several statistical tests, such as Chi
 * square, G square. In this version we only provide the Bayesian test
 * implementation.
 * 
 * @author fschluter
 * 
 */
public interface StatisticalIndependenceTest {

	/**
	 * @param dataset
	 *            An instance of the dataset class
	 * @param x
	 *            A random variable
	 * @param y
	 *            A random variable
	 * @param z
	 *            A set of random variables
	 * @param i
	 *            The cardinality of all the variables (i.e. 2 for binary
	 *            variables).
	 * @return A decision of Independences of the triplet, given data
	 */
	public boolean independent(Dataset dataset, List<Integer> x, List<Integer> y, List<Integer> z);

	/**
	 * @param dataset
	 *            An instance of the dataset class
	 * @param triplet
	 *            A triplet of sets of variables for querying conditional
	 *            independence
	 * @return
	 */
	public boolean independent(Dataset dataset, IndependenceTriplet triplet);

	/**
	 * @return A vector of double values, wit loglikelihoods[0] the log
	 *         likelihood of independence, and logprobs[1] the log likelihood of
	 *         dependence
	 */
	public double[] getLoglikelihoods();

	/**
	 * @return A vector of double values, wit logprobs[0] the log probability of
	 *         independence, and logprobs[1] the log probability dependence
	 */
	public double[] getLogProbs();
}
