package graphs;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple data structure to represent an undirected graph with n nodes (one per
 * each random variable of the domain).
 * 
 * @author fschluter
 * 
 */
public class UndirectedGraph {

	/* Number of nodes of the graph */
	private int n;

	/* Maps the edges (i,j) in the graph */
	public boolean adjMatrix[][];

	/**
	 * Creates an undirected graph with n nodes and no edges
	 * 
	 * @param n
	 */
	public UndirectedGraph(int n) {
		this.n = n;
		adjMatrix = new boolean[n][n];
	}

	/**
	 * Creates an undirected graph with n nodes and no edges
	 * 
	 * @param n
	 * @param fully
	 */
	public UndirectedGraph(int n, boolean fully) {
		this.n = n;
		adjMatrix = new boolean[n][n];
		for (int i = 0; i < n - 1; i++) {
			for (int j = i + 1; j < n; j++) {
				if (fully) {
					adjMatrix[i][j] = true;
					adjMatrix[j][i] = true;
				} else {
					adjMatrix[i][j] = false;
					adjMatrix[j][i] = false;
				}
			}
		}

	}

	/**
	 * @param g
	 *            UndirectedGraph object to copy
	 */
	public UndirectedGraph(UndirectedGraph g) {
		n = g.n;
		adjMatrix = new boolean[n][n];
		for (int i = 0; i < n - 1; i++) {
			for (int j = i + 1; j < n; j++) {
				adjMatrix[i][j] = g.adjMatrix[i][j] ? true : false;
				adjMatrix[j][i] = g.adjMatrix[j][i] ? true : false;
			}
		}

	}

	/**
	 * Flips an edge between nodes i and j.
	 * 
	 * @param i
	 * @param j
	 */
	public void flipEdge(int i, int j) {
		adjMatrix[i][j] = !adjMatrix[i][j];
		adjMatrix[j][i] = !adjMatrix[j][i];
	}

	/**
	 * Flips an edge between nodes i and j.
	 * 
	 * @param i
	 * @param j
	 */
	public void addEdge(int i, int j) {
		adjMatrix[i][j] = true;
		adjMatrix[j][i] = true;
	}

	/**
	 * Returns the Markov blanket of a variable (this is the set of adjacent variables in the graph)
	 * @param x Variable which we want to know its adjacencies set
	 * @return The Markov blanket of a variable (this is the set of adjacent variables in the graph).
	 */
	public List<Integer> getBlanketOf(int x) {
		List<Integer> blanketOfX = new ArrayList<Integer>();
		for (int i = 0; i < n - 1; i++)
			for (int j = i + 1; j < n; j++) {
				if (i == x && adjMatrix[i][j])
					blanketOfX.add(j);
				if (j == x && adjMatrix[i][j])
					blanketOfX.add(i);
			}
		return blanketOfX;
	}

	/**
	 * Returns true or false whenever there exist an edge between variables x and y 
	 * @param x First variable
	 * @param y Second variable
	 * @return Returns true or false whenever there exist an edge between variables x and y
	 */
	public boolean existEdge(int x, int y) {
		if (x < y)
			return adjMatrix[x][y];
		else
			return adjMatrix[y][x];
	}

	public int getNumberOfNodes() {
		return n;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String toString = "";
		for (int i = 0; i < n; i++) {
			toString += i + "\t";
			for (int j = 0; j < n; j++) {
				if (i != j) {
					if (adjMatrix[i][j])
						toString += j + " ";
				}
			}
			toString += "\n";
		}
		return toString;
	}
}
