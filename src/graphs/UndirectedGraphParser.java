package graphs;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Allows to read a graph from data. It assumes that the representation of the
 * graph is the same that returns the toString() method of the
 * graphs.UndirectedGraph class. 
 * 
 * @author fschluter
 * 
 */
public class UndirectedGraphParser {

	/**
	 * @param datasetName
	 * @param numberOfNodes
	 * @return
	 */
	public static UndirectedGraph readTrueGraph(String datasetName, int numberOfNodes) {
		UndirectedGraph trueGraph = new UndirectedGraph(numberOfNodes);
		try {
			BufferedReader br;
			br = new BufferedReader(new FileReader(datasetName + ".graph"));
			String line = br.readLine();
			int row = 0;
			while (line != null) {
				String[] splitedLine = line.split(" ");
				for (int i = 0; i < splitedLine.length; i++) {
					trueGraph.adjMatrix[row][i] = splitedLine[i].equals("1") ? true : false;
				}
				line = br.readLine();
				++row;
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return trueGraph;
	}

}
